import urllib.request
import tarfile
import pydicom
from pydicom.data import get_testdata_files
from pydicom.filereader import dcmread
from os import listdir, makedirs
from os.path import isfile, join, dirname
import shutil
import pandas as pd
import getpass
import time


def dl_and_ext(url, local_path, dir_name):
    # download and extract the zipped file
    # urllib.request.urlretrieve(url, local_path)
    tf = tarfile.open(local_path)
    tf.extractall(path=dir_name)


def reorganizer(dir_name, df1, df2):
    # per each DICOM file, extract wanted information into df's and reorder files into a directory structure
    for filename in listdir(dir_name):
        dicom_file = join("/", dir_name, filename)
        ds = pydicom.read_file(dicom_file)
        df1 = df1.append(
            pd.Series([ds.PatientName, ds.PatientAge, ds.PatientSex, ds.InstitutionName], index=df1.columns),
            ignore_index=True)
        df2 = df2.append(pd.Series([ds.PatientName, ds.StudyInstanceUID, ds.SeriesInstanceUID, ds.AcquisitionTime],
                                   index=df2.columns), ignore_index=True)
        new_file_dir = r'C:\Users\{}\viz.AI\{}\{}\{}\{}'.format(getpass.getuser(), ds.PatientName, ds.StudyInstanceUID,
                                                                ds.SeriesInstanceUID, filename)
        makedirs(dirname(new_file_dir), exist_ok=True)
        shutil.move(dicom_file, new_file_dir)
    return df1, df2


def unique_patients(df1):
    # parser age into integer
    df1['age'] = df1['age'].str.rstrip('Y')
    df1['age'] = df1['age'].astype(int)
    # display unique patient details
    unique_df1 = df1.drop_duplicates()
    unique_df1 = unique_df1.reset_index()
    print(unique_df1[['name', 'age', 'sex']])


def avg_ct_time(df2, scan_duration_df):
    # calculates the average time for each CT scan, displayed in seconds (reffering to the average time of each SeriesInstanceUID, corresponding to a single scan)
    unique_df2 = df2.drop_duplicates()
    for SeriesInstanceUID in unique_df2.SeriesInstanceUID:
        name = df2[df2['SeriesInstanceUID'] == SeriesInstanceUID]['name'].iloc[0]
        max_acq_time = df2[df2['SeriesInstanceUID'] == SeriesInstanceUID]['acquistion_time'].max()
        min_acq_time = df2[df2['SeriesInstanceUID'] == SeriesInstanceUID]['acquistion_time'].min()
        scan_duration_df = scan_duration_df.append(
            pd.Series([name, SeriesInstanceUID, max_acq_time, min_acq_time], index=scan_duration_df.columns),
            ignore_index=True)
    scan_duration_df = scan_duration_df.drop_duplicates()
    scan_duration_df['time_diff'] = pd.to_numeric(scan_duration_df['max_acquistion_time']) - pd.to_numeric(
        scan_duration_df['min_acquisiton_time'])
    return scan_duration_df['time_diff'].mean()


def unique_hosp(df1):
    # display amount of different hospitals from which the data came
    return df1.hospital_name.nunique()


def main():
    # constants
    url = r'https://s3.amazonaws.com/viz_data/DM_TH.tgz'
    local_path = r'C:\Users\{}\viz.AI\DM_TH.tgz'.format(getpass.getuser())
    dir_name = r'C:\Users\{}\viz.AI\DICOM'.format(getpass.getuser())
    dl_and_ext(url, local_path, dir_name)
    # create all needed dataframes
    df1 = pd.DataFrame(columns=['name', 'age', 'sex', 'hospital_name'])
    df2 = pd.DataFrame(columns=['name', 'StudyInstanceUID', 'SeriesInstanceUID', 'acquistion_time'])
    scan_duration_df = pd.DataFrame(columns=['name', 'SeriesInstanceUID', 'max_acquistion_time', 'min_acquisiton_time'])
    df1, df2 = reorganizer(dir_name, df1, df2)
    unique_patients(df1)
    avg_ct_time(df2, scan_duration_df)
    unique_hosp(df1)


main()